﻿using ALab4.Code;

namespace ALab4;

public static class Program
{
    public static void Main(string[] args)
    {
        var dir = new DirectoryInfo("./Data");
        Console.WriteLine("=======================");
        foreach (var file in dir.GetFiles())
        {
            Console.WriteLine($"FILE: {file.Name}");
            var taskData = TaskReader.Read(file.FullName);
            var app = new App(taskData);
            Console.WriteLine("STOCK");
            app.Run();
            //Console.WriteLine("CUSTOM");
            //app.Run();
            Console.WriteLine("=======================");
        }
    }
}