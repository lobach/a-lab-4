﻿using System.Numerics;

namespace ALab4.Code;

public static class TaskReader
{
    public static TaskData Read(string fileName)
    {
        var lines = File.ReadAllLines(fileName);
        var points = new List<Vector2>();
        foreach (var line in lines)
        {
            var values = line
                .Replace('\t', ' ')
                .Split(' ')
                .ToArray();
            points.Add(new Vector2(values[1].ToFloat(), values[2].ToFloat()));
        }

        return new TaskData
        {
            Points = points
        };
    }
}