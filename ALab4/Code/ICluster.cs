﻿using System.Numerics;

namespace ALab4.Code;

public interface ICluster
{
    public Vector2 Position { get; }
    public PathData? SolveData { get; }
    public int ClustersCount { get; }
    public List<ICluster> Clusters { get; }
    public void Solve(ICluster? enter = null, ICluster? exit = null);
    public void AddCluster(ICluster cluster);
    public void Reduce();
    public ICluster ClosestTo(ICluster cluster, ICluster? ignore = null);
    public float Distance();
    public List<ICluster> Path();
}