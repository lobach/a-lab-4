﻿namespace ALab4.Code;

public class PathData
{
    public ICluster Enter { get; set; }
    public ICluster Exit { get; set; }

    public List<ICluster> Path { get; init; }

    public PathData() =>
        Path = new List<ICluster>();
}