﻿using System.Numerics;

namespace ALab4.Code;

public class TaskData
{
    public int Size => Points.Count;
    public List<Vector2> Points { get; init; }
}