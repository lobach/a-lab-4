﻿using System.Numerics;

namespace ALab4.Code;

public class Point : ICluster
{
    public Vector2 Position { get; }
    public PathData? SolveData { get; private set; }
    public int ClustersCount => 1;
    public List<ICluster> Clusters { get; }

    public Point(Vector2 position)
    {
        Position = position;
        Clusters = new List<ICluster> {this};
    }

    public void Solve(ICluster? enter = null, ICluster? exit = null)
    {
        SolveData = new PathData
        {
            Enter = this,
            Exit = this,
            Path = new List<ICluster> {this}
        };
    }

    public void AddCluster(ICluster cluster)
    {
    }

    public void Reduce()
    {
    }

    public ICluster ClosestTo(ICluster cluster, ICluster? ignore = null) =>
        this;

    public float Distance() =>
        0.0f;

    public List<ICluster> Path() =>
        new() {this};
}